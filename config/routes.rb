Taskminator::Engine.routes.draw do
  resources :scheduled_tasks, only:[:index]
  resources :scheduled_task_logs, only:[:index]
  resources :tasks, only:[:index]
end
