require "taskminator/engine"

module Taskminator
  class TaskScheduler
    def self.run_scheduled_tasks
      now = Time.now.utc
      Rails.logger.info "Starting scheduled task at: #{now}"
      Taskminator::ScheduledTask.all.each do |st|
        self.run_scheduled_task(st, now)
      end
    end
    private
    def self.run_scheduled_task(scheduled_task, time_now)
      Rails.logger.info "** Processing scheduled task id: " + scheduled_task.id.to_s + " **"  
      task = scheduled_task.task    
      Rails.logger.info "Task name: " + task.name
      Rails.logger.info "Time now: " + time_now.to_s 
    
      last_run = scheduled_task.last_run.try(:start)
       
      if ( last_run )
        Rails.logger.info "Task last ran at: " +  last_run.to_s
      else
        Rails.logger.info "Task never been run"
        # if task has never run, give it a time at the epoch (1970) 
        last_run = Time.at(0)
      end
    
      frequency = scheduled_task.frequency
        
      if determine_whether_to_run( frequency, scheduled_task.time_of_day, scheduled_task.month_of_year, scheduled_task.day_of_month,scheduled_task.day_of_week, last_run, time_now )
                    
        # The method on the task object tries to set the task to running and will return true if rows are affected by the update.
        # If no rows are updated it means that it was already running. 
        # We don't want to have two instances of same task running at the same time 
        # Without this check this may have happenned if run_scheduled_tasks (all tasks) takes longer than the cron schedule interval,
        # causing overlap.
        if task.set_running_if_not_already
          #task.set_running_if_not_already
          Rails.logger.info "Running task..."
        
          scheduled_task_log = Taskminator::ScheduledTaskLog.new
          scheduled_task_log.scheduled_task_id = scheduled_task.id
          scheduled_task_log.start = Time.now
          scheduled_task_log.save
    
          begin    
            # call the task using ruby reflection.
            #Rails.logger.info "======================="
            #Rails.logger.info scheduled_task.params
            #Rails.logger.info "======================="
            task.klass.constantize.send(task.method, scheduled_task.params)
            scheduled_task_log.success = true
            Rails.logger.info "success"
          rescue Exception => e
            scheduled_task_log.end = Time.now
            scheduled_task_log.success = false
            scheduled_task_log.info = "#{e} : #{e.to_s}"
            scheduled_task_log.save
            Rails.logger.error "Error: " + e.to_s
          ensure
            scheduled_task_log.end = Time.now
            scheduled_task_log.save
            # set to not running anymore
            task.is_running = false
            task.save     
            Rails.logger.info "Task completed at: " + scheduled_task_log.end.to_s
          end
          
        else
          Rails.logger.info "Task is still running from a previous time!"
          # still running.  
          # TODO: alert someone?
        end
    
      else
          Rails.logger.info "Not running task this time."
      end
    end

    # work out whether to run a task, given its frequency, last run date and time of day (in mins past midnight)
    def self.determine_whether_to_run( frequency, time_of_day_to_run, month_to_run,day_of_month_to_run,day_of_week_to_run, last_run, time_now )    
    
      # if it is set to run asap, just let it run,
      # otherwise, do some calculations...
            
      if (frequency == "ASAP")
        Rails.logger.info "running asap"
        return true
      else       
        return compare_times(frequency, time_of_day_to_run, month_to_run, day_of_month_to_run, day_of_week_to_run, last_run, time_now)                     
      end
    end
    def self.compare_times(frequency, time_of_day_to_run, month_to_run, day_of_month_to_run, day_of_week_to_run, last_run, time_now )
    
      if Taskminator::ScheduledTask.frequencies[frequency] >= Taskminator::ScheduledTask.frequencies["ONCE"]
      
        # For these frequencies, we compare to midnight on the last run date, to try to honour the 
        # time of day to run...
        seconds_since_midnight_last_run = ( (last_run.hour*60*60) + (last_run.min*60) + (last_run.sec) )  
        last_run_midnight = last_run - seconds_since_midnight_last_run
        Rails.logger.info "Comparing time now to: " + last_run_midnight.to_s
        case frequency
          when "ONCE"
            Rails.logger.info "Frequency: once"
            return (last_run_midnight <= Time.at(0)) && time_now.to_i >= time_of_day_to_run
          when "DAILY"
            Rails.logger.info "Frequency: daily"
            return (last_run_midnight < time_now - (1.day)) && check_minutes_past_midnight(time_of_day_to_run, time_now)
          when "WEEKLY"
            Rails.logger.info "Frequency: weekly"
            return (last_run_midnight < time_now - (1.week)) && day_of_week_to_run <= time_now.wday && check_minutes_past_midnight(time_of_day_to_run, time_now)
          when "FORTNIGHTLY"
            Rails.logger.info "Frequency: fortnightly"
            return (last_run_midnight < time_now - (2.weeks)) && check_minutes_past_midnight(time_of_day_to_run, time_now)
          when "MONTHLY"
            Rails.logger.info "Frequency: monthly"
            return check_month_year(last_run,time_now) && check_day(day_of_month_to_run,time_now) && check_minutes_past_midnight(time_of_day_to_run, time_now)
          when "YEARLY"
            Rails.logger.info "Frequency: yearly"
            return (last_run_midnight.change(month:month_to_run,day:day_of_month_to_run) < time_now - (1.year)) && check_minutes_past_midnight(time_of_day_to_run, time_now)
          else 
            Rails.logger.info "UNKNOWN FREQUENCY!"
            return false
        end        
        
      else # i.e. more frequent than daily.
    
        case frequency
           when "HALF_HOURLY"
              Rails.logger.info "Frequency: half hourly"
              return (last_run < time_now - (30.minutes))
            when "HOURLY"
              Rails.logger.info "Frequency: hourly"
              return (last_run < time_now - (1.hour))
            when "FOUR_TIMES_A_DAY"
              Rails.logger.info "Frequency: four times a day"
              return (last_run < time_now - (6.hours))
            when "TWICE_A_DAY"
              Rails.logger.info "Frequency:twice a day"
              return (last_run < time_now - (12.hours))
            else 
              Rails.logger.info "UNKNOWN FREQUENCY!"
              return false
        end # end case
      
      end # end if-else
    end # method
    def self.check_minutes_past_midnight(time_of_day_to_run, time_now)
      current_mins_past_midnight = calculate_current_minutes_past_midnight(time_now)   
      Rails.logger.info "Current minutes past midnight: " + current_mins_past_midnight.to_s
      Rails.logger.info "Minutes past midnight to run: " + time_of_day_to_run.to_s
      return current_mins_past_midnight > time_of_day_to_run
    end
    def self.calculate_current_minutes_past_midnight(time_now)
      (time_now.hour*60) + time_now.min
    end
    def self.check_month_year(last_run,time_now)
      Date.new(time_now.year,time_now.month) > Date.new(last_run.year,last_run.month)
    end
    def self.check_day(day_of_month_to_run, time_now)
      cur_day = time_now.day
      eom_day = time_now.end_of_month.day
      #allow to run if assigned day of month to run is the current day or if it is already the end of month
      (cur_day >= day_of_month_to_run) || ((eom_day == cur_day) && (eom_day < day_of_month_to_run))
    end
  end
end
