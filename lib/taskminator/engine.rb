module Taskminator
  class Engine < ::Rails::Engine
    isolate_namespace Taskminator
  end
end
