require 'test_helper'

module Taskminator
  class ScheduledTasksControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    test "should get index" do
      get scheduled_tasks_index_url
      assert_response :success
    end

  end
end
