require 'test_helper'

module Taskminator
  class TasksControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    test "should get index" do
      get tasks_index_url
      assert_response :success
    end

  end
end
