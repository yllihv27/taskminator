require 'test_helper'

module Taskminator
  class ScheduledTaskLogsControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    test "should get index" do
      get scheduled_task_logs_index_url
      assert_response :success
    end

  end
end
