class CreateTaskminatorTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :taskminator_tasks do |t|
      t.string :name
      t.string :klass
      t.string :method
      t.boolean :is_running, default:false

      t.timestamps
    end
  end
end
