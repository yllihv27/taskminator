class CreateTaskminatorScheduledTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :taskminator_scheduled_tasks do |t|
      t.integer :task_id
      t.string :params
      t.integer :frequency
      t.integer :month_of_year,default:1
      t.integer :day_of_month,default:1
      t.integer :day_of_week,default:1
      t.integer :time_of_day,default:0

      t.timestamps
    end
  end
end
