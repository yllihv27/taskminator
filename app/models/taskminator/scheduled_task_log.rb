module Taskminator
  class ScheduledTaskLog < ApplicationRecord
    belongs_to :scheduled_task
  end
end
