module Taskminator
  class ScheduledTask < ApplicationRecord
    enum frequency: %w( ASAP HALF_HOURLY HOURLY FOUR_TIMES_A_DAY TWICE_A_DAY ONCE DAILY WEEKLY FORTNIGHTLY MONTHLY YEARLY)
    belongs_to :task
    has_many :scheduled_task_logs
    serialize :params, Hash

    def last_run
       scheduled_task_logs.last
    end

    def time
      #time_of_day = self.time_of_day * 60 unless self.ONCE? && self.time_of_day
      self.ONCE? ? Time.at(time_of_day).strftime("%Y-%m-%d %I:%M%p") : Time.at(time_of_day*60).strftime("%I:%M%p")
    end
  end
end
