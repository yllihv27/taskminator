module Taskminator
  class Task < ApplicationRecord
    has_many :scheduled_tasks
    # instance method to set task running if it isn't already.
    def set_running_if_not_already
      # Note that connection.update returns the number of rows affected.
      # In this way, we can tell whether it was already running. 
      self.is_running = true
      if is_running_changed?
        save
        true
      else
        false
      end
    end
  end
end
