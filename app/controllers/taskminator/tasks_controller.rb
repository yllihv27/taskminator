require_dependency "taskminator/application_controller"

module Taskminator
  class TasksController < ApplicationController
    def index
      @tasks = Taskminator::Task.all
    end
  end
end
