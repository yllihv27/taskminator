require_dependency "taskminator/application_controller"

module Taskminator
  class ScheduledTaskLogsController < ApplicationController
    def index
      @scheduled_task_logs = Taskminator::ScheduledTaskLog.all.includes(scheduled_task:[:task])
      @scheduled_task_logs = @scheduled_task_logs.where(scheduled_task_id:params[:scheduled_task_id])
    end
  end
end
