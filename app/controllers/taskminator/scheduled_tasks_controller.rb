require_dependency "taskminator/application_controller"

module Taskminator
  class ScheduledTasksController < ApplicationController
    def index
      @scheduled_tasks = Taskminator::ScheduledTask.all.includes(:task,:scheduled_task_logs)
    end
  end
end
