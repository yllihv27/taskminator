$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "taskminator/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "taskminator"
  s.version     = Taskminator::VERSION
  s.authors     = ["vhilly santiago"]
  s.email       = ["vhilly.santiago@icloud.com"]
  s.homepage    = "https://getzclinical.com"
  s.summary     = "Simple Task Scheduler"
  s.description = "Simple Task Scheduler based on  RicSwirrl's Taskit"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.2"

  s.add_development_dependency "rspec-rails", "~> 3.4", ">= 3.4.2"
  s.add_development_dependency "factory_girl_rails", "~> 4.7"
  s.add_development_dependency "annotate", "~> 2.7", ">= 2.7.2"
  s.add_development_dependency "sqlite3"
end
