# This migration comes from taskminator (originally 20171010002000)
class CreateTaskminatorScheduledTaskLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :taskminator_scheduled_task_logs do |t|
      t.integer :scheduled_task_id
      t.datetime :start
      t.datetime :end
      t.boolean :success
      t.string :info

      t.timestamps
    end
  end
end
